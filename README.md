#How to (really)
    1. clone this repo
    2. have python 3.+  and Google Cloud SDK CLI installed and set up
    3. run _sudo python3 setup.py install (or develop)_
    4. put sql files into _./resources/sql_queries/_ dir (one file == one query)
    5. run _python3 bq_script.py_
    6. when finished, output data in .xlsx file can be found in _./output/_ folder