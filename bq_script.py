#!/usr/bin/python3

from helpers.file_ops import load_json, load_file, get_file_list
from helpers.parsers import parse
from helpers.big_query import oneline_query, run_query, replace_select_parts
from helpers.console import get_select_parts_to_change
from helpers.date_time import timestamp
from helpers.excel import save_query_output

config = load_json('./config.json')


def main():
    # get to_cut via console prompt
    to_cut = get_select_parts_to_change()
    tstamp = timestamp()

    print('Getting query filenames ....')
    queries = get_file_list(config['paths']['sql_queries'])

    for query in queries:
        # load formatted sql query and oneline it
        # IMPORTANT - manually remove comments in sql file - this will not
        # take care of that
        print('Started processing "{}":'.format(query))
        load_path = ''.join([config['paths']['sql_queries'], query])

        raw_query = load_file(load_path)

        print('--> Transforming multiline query to oneline format...')
        onelined_query = oneline_query(raw_query)
        onelined_query = replace_select_parts(to_cut, onelined_query)

        # run bq query as process and save output as .txt
        print('--> Calling Big Query now...')
        run_query(onelined_query)

        # load raw .txt content and parse_csv it
        print('--> Parsing returned text data...')
        raw_bq_output = load_file(config['paths']['temp'])
        bq_output = parse(raw_bq_output)

        save_query_output(config['paths']['output'], tstamp, query, bq_output)

        print('--> "{}" finished.\n'.format(query))


if __name__ == "__main__":
    main()
