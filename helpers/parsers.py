"""
Contains parsing functions
"""


def parse(txt_table: str):
    """
    parses txt bq output table into .csv suitable data
    """
    try:
        output = []

        # break text content into list, each item represent a line
        # cut off first two lines of the document which
        # contain blank line and +------+-------+ frame of the output
        # then take the rest of the rows, except for the last line
        lines = txt_table.splitlines()
        lines = [lines[2]] + lines[4: -1]

        # break lines into list with individual items
        # remove empty items, nonempty will be stripped of whitespaces
        for line in lines:
            items = line.split('|')
            items = [item.strip() for item in items if item is not '']
            output.append(items)

        return output
    except Exception as e:
        print('Error when parsing BQ output: {}'.format(str(e)))


def parse_json(txt_table: str):
    """
    parses txt bq output into json format dict
    not used yet, but nice excercise :)
    """
    output = {}
    temp = parse(txt_table)

    for i, row in enumerate(temp):
        for x, each in enumerate(row):

            if i > 0:
                if (x == 0) and (each not in list(output.keys())):
                    output[each] = []

                else:
                    tdict = dict(
                        zip(
                            [each for each in temp[0][1:]],
                            [each for each in row[1:]]
                        )
                    )

                    output[row[0]].append(tdict)
                    break

            else:
                continue

    return output
