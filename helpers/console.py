"""
Handles shell inputs
"""

from sys import exit
import re


def get_select_parts_to_change():
    def input_(prompt: str):
        return input(prompt)

    def verify(user_input: str):
        # weak, very simple verification
        # just to make sure no really wrong values are put in
        regexes = {
            "date": r"^2019[0|1][0-9][0-3][0-9]$",
            "country": r"^\d{9}$",
            "dealer": r"(^\d{0,3}-{1}\d{3,5}\w?$)|(^$)"
        }

        for value in list(regexes.values()):
            if re.fullmatch(value, user_input):
                return user_input

            else:
                continue

        return verify(
            input_('\n"{}" has not correct format.'
                   'Try again or press CTRL+C to abort script.: '.format(
                    user_input))
        )

    def get():
        print(
            '''Type in "START date", in format: >>yyyymmdd<<.
Type the date exactly as suggested, otherwise the query will not work!
            '''
        )
        start_date = verify(input_('\nEnter "Start Date": '))

        print(
            '''\nType in "END date", in format: >>yyyymmdd<<.
Type the date exactly as suggested, otherwise the query will not work!
            '''
        )
        end_date = verify(input_('\nEnter "End Date": '))

        print(
            """
            \nType in code for dealer. Must be EXACT, otherwise query will not work.
Leave BLANK, if no dealer is needed.
            """
        )
        dealer = verify(input_('\nEnter "Dealer Code", otherwise just press ENTER: '))

        print(
            '''
            \nType in "COUNTRY code in BigQuery table", if selecting from
other country, then it is specified in the query. Otherwise just press ENTER.
Format is: >>nnnnnnnn<<
            '''
        )
        country = verify(input_('\nEnter "Country n digit country code. '
                                'Press ENTER, if selecting from default country: '))

        return [start_date, end_date, dealer, country]

    def confirm(inputs: list):

        print('\nIs this what you want?')
        print('START DATE: "{}"'.format(inputs[0]))
        print('END DATE: "{}"'.format(inputs[1]))
        print('DEALER CODE: "{}"'.format(inputs[2]))
        print('COUNTRY CODE: "{}"'.format(inputs[3]))

        choice = input('Do you want to start running queries? [y/n]: ')

        if 'y' in choice.strip().lower():
            return tuple(input_.strip() for input_ in inputs)

        else:
            exit('Terminated. Run script again.')

    return confirm(get())
