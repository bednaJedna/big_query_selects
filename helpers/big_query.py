"""
Handles biq query
"""
from os import system
import re
from helpers.file_ops import load_json

config = load_json('./config.json')


def replace_select_parts(inputs: tuple, query: str):
    dates = [inputs[0], inputs[1]]
    country = ''.join(['mercuryskodadata.', inputs[3]])
    dealer = inputs[2]

    date_pattern = re.compile(r"[0-9]{8}")
    country_pattern = re.compile(r"mercuryskodadata\.\d{9}")
    dealer_pattern = re.compile(r"\d{0,3}-{1}\d{3,5}\w?")

    # starting with dates
    def replace_dates(dates: list, query: str):
        found_patterns = re.findall(date_pattern, query)
        found_patterns = [pattern for pattern in found_patterns[-2:] if len(found_patterns) > 2]

        for i, pattern in enumerate(found_patterns):
            query = query.replace(pattern, dates[i])

        return query

    # continue with country
    def replace_country(country: str, query: str):
        found_patterns = re.findall(country_pattern, query)

        for pattern in found_patterns:
            query = query.replace(pattern, country)

        return query

    # finish with dealer
    def replace_dealer(dealer: str, query: str):
        found_patterns = re.findall(dealer_pattern, query)

        for pattern in found_patterns:
            query = query.replace(pattern, dealer)

        return query

    query = replace_dates(dates, query)
    query = replace_country(country, query) if inputs[3] else query
    query = replace_dealer(dealer, query) if dealer else query

    return query


def oneline_query(raw_query: str):
    """
    reformats multiline sql query to oneline for bq cli
    """
    oneline_comment = re.compile(r"--\s*.*")
    multiline_comment = re.compile(r"\/\*.*\*\/")

    lines = raw_query.splitlines()

    # remove oneline comments while still each line is 
    # as nested list
    for i, line in enumerate(lines):
        cuts = re.findall(oneline_comment, line)

        if line.strip().startswith('#'):
            del lines[i]

        elif len(cuts) > 0:
            for cut in cuts:
                lines[i] = lines[i].replace(cut, '')

                # convert nested list of lines to one string
    lines = [''.join([line.strip(), ' ']) for line in lines]

    content = ''.join(line for line in lines)
    content = content.replace("'", '"')

    # remove multiline comments
    multicuts = re.findall(multiline_comment, content)

    if multicuts:
        for cut in multicuts:
            content = content.replace(cut, '')

        return content

    else:
        return content


def run_query(query: str):
    try:
        system(
            "bq query --use_legacy_sql=false '{query}' > {output}".format(
                query=query,
                output=config['paths']['temp']
            )
        )
    except Exception as e:
        print('Error in query: {}'.format(str(e)))
