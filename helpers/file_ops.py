"""
Handles working with files
"""
import csv
import json
from os import listdir


def load_file(filepath: str):
    with open(filepath, mode='r', encoding='utf-8') as file:
        return file.read()


def save_csv(filepath: str, data: list):
    with open(filepath, mode='w', encoding='utf-8') as file:
        try:
            writer = csv.writer(file, delimiter=',')
            for row in data:
                writer.writerow(row)
        except Exception as e:
            print('Error when saving CSV file: {}'.format(str(e)))


def save_json(filepath: str, data: dict):
    with open(filepath, mode='w', encoding='utf-8') as file:
        json.dump(data, file, ensure_ascii=False, indent=2)


def load_json(filepath: str):
    with open(filepath, mode='r', encoding='utf-8') as file:
        return json.load(file)


def get_file_list(path: str):
    """
    checks folder with sql queries files and returns list with fienames
    we expect, that each file contain one query
    """
    files = listdir(path)

    return [file for file in files if file.endswith('.txt')]
